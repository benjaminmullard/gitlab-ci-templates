# Changelog

## v13.0.2 (2022-11-10)

### Fixed

- Updated `pa11y_ci` job to `gitlab-pa11y-ci:7.0.1`.
- Updated `lint_docker` job to `hadolint:2.12.0`.
- Updated `node_sbom` job to `cyclonedx-npm@1.4.1`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.7`.

## v13.0.1 (2022-11-04)

### Fixed

- Updated `pagean` job to `pagean:8.0.1`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.4`.

## v13.0.0 (2022-11-02)

### Changed

- BREAKING: Updated `pagean` job to `pagean:8.0.0`.
- BREAKING: Updated `pa11y_ci` job to `gitlab-pa11y-ci:7.0.0`.
- BREAKING: Updated `.dind` template to latest Docker 20 images with pinned tags. (#210)

### Fixed

- Updated `release_check` job to clone the full repository to avoid false negative tests where no git tags are found in a shallow clone. (#211)
- Updated `prepare_release` job to `gitlab-releaser@4.0.3`.
- Updated `node_sbom` job to `cyclonedx-npm@1.3.0`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.3`.
- Updated `.python` template to `python:3.11.0`.

## v12.0.0 (2022-10-23)

### Changed

- BREAKING: Moved `node_sbom` job to new [`@cyclonedx/cyclonedx-npm` module](https://www.npmjs.com/package/@cyclonedx/cyclonedx-npm), v1.2.0. See details on the deprecated [@cyclonedx/bom@4.0.0](https://github.com/CycloneDX/cyclonedx-node-module/releases/tag/v4.0.0) package. This changes several defaults and CLI arguments. See [the module documentation](https://www.npmjs.com/package/@cyclonedx/cyclonedx-npm) for details, and [the `node_sbom` job](jobs/Node-Sbom.gitlab-ci.yml) for new variables and configuration. The updated job produces an equivalent SBOM, but per v1.4 of the SBOM spec (now the default, updated from v1.3).
- BREAKING: Updated `Node-Version-Tests` to include tests with Node 19. (#207)
- BREAKING: Updated Node LTS `jobs` and `templates` from Node 16 to 18 per the [Node release schedule](https://github.com/nodejs/Release). (#208)
- BREAKING: Updated overrides to GitLab templates for changes in GitLab 15.5, which includes adding C# and HTML SAST analysis with `semgrep`. (#209)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.2`.
- Updated `.python` template to `python:3.10.8`.

## v11.2.1 (2022-10-12)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.19.0`.
- Updated `lint_lockfile` job to `lockfile-lint@4.9.6`.
- Updated `prepare_release` job to GitLab `release-cli:v0.14.0`

## v11.2.0 (2022-09-30)

### Changed

- Added `node_lts_test_win_11` job using a group-level Windows 11 runner to
  `/jobs/Node-LTS-Test-Win.gitlab-ci.yml`. The two Windows jobs are mutually
  exclusive. By default, the `node_lts_test_win` job will still be run. Set
  `USE_WINDOWS_11_RUNNER` to `true` to run the `node_lts_test_win_11` job
  instead. Note this runner is only available to jobs in this group. (#206)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.18.0`.
- Updated `lint_lockfile` job to `lockfile-lint@4.9.5`.

## v11.1.4 (2022-09-25)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.17.2`.
- Updated `pa11y_ci` job to `gitlab-pa11y-ci:6.1.1`.

## v11.1.3 (2022-09-16)

### Fixed

- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.15.0`.
- Updated `lint_yaml` job to `yamllint@1.28.0`.

## v11.1.2 (2022-09-11)

### Fixed

- Updated `Web-UI-Tests-With-Lint` collection to specify `npm_install` as `needs` for `lint_css` and `lint_html`. (#203)

## v11.1.1 (2022-09-07)

### Fixed

- Updated `prepare_release` job to `gitlab-releaser@4.0.2`.
- Updated `pa11y_ci` job to `gitlab-pa11y-ci:6.1.0`.
- Updated `lint_md` job to `markdownlint-cli@0.32.2`.
- Updated `node_sbom` job to `@cyclonedx/bom@3.10.6`.
- Updated `lint_nunjucks` job and `.djlint` template to `djlint@1.12.3`.
- Updated `.python` template and all Python jobs to `python:3.10.7-alpine3.16`.

## v11.1.0 (2022-08-12)

### Changed

- Pin `yamllint` version in [`lint_yaml`](./jobs/Lint-Yaml.gitlab-ci.yml) job, and update to use default config file instead of variable. (#198, #200)
- Added [`.python`](./templates/Python.gitlab-ci.yml) template as base for all Python jobs (`.djlint`, `lint_nunjucks`, `lint_yaml`), with the python image as well as standard `pip` settings. (#198, #201)
- Updated `sokrates` job to expose the report artifacts in merge requests and added artifical metrics report to collect release evidence. (#192)

### Fixed

- Updated `lint_lockfile` job to `lockfile-lint@4.8.0`.

## v11.0.0 (2022-08-05)

### Changed

- BREAKING: Updated `pa11y-ci` job to [`gitlab-pa11y-ci` v6.0.0](https://gitlab.com/gitlab-ci-utils/gitlab-pa11y-ci/-/releases/6.0.0) image.
- BREAKING: Update `pagean` job to [`pagean@7.0.0`](https://gitlab.com/gitlab-ci-utils/pagean/-/releases/7.0.0)
- Added [`lint_nunjucks`](./jobs/Lint-Nunjucks.gitlab-ci.yml) job to lint [Nunjucks files](https://mozilla.github.io/nunjucks/) with `djlint`. (#196)
- Added [`.djlint`](./templates/Djlint.gitlab-ci.yml) template to lint various formats with `djlint`. (#196)

### Fixed

- Updated `prepare_release` job to `gitlab-releaser@4.0.1`
- Updated `lint_md` job to `markdownlint-cli@0.32.1`
- Updated `lint_lockfile` job to `lockfile-lint@4.7.7`.

### Miscellaneous

- Update renovate config to manage `pip` package versions installed in templates. (#196)

## v10.0.0 (2022-07-21)

### Changed

- BREAKING: Updated `npm_check` job to `npm-check@6.0.1`.
- BREAKING: Updated `lint_md` job to `markdownlint-cli@0.32.0` and updated default configurastion with new rules.
- BREAKING: Updated `prepare_release` job to GitLab `release-cli:v0.13.0`

### Fixed

- Updated `lint_lockfile` job to `lockfile-lint@4.7.6`.
- Updated `node_sbom` job to `@cyclonedx/bom@3.10.4`.

## v9.1.0 (2022-06-23)

### Changed

- Added [Sokrates](https://www.sokrates.dev/) job to `all-projects-slim` template to continue to gather data from a variety of projects for evaluation. The job allows failure, so not breaking. (#191)

### Fixed

- Updated GitLab template overrides for changes in GitLab 15.1. (#188)
- Updated `node_sbom` job to `@cyclonedx/bom@3.10.1`

## v9.0.0 (2022-06-17)

### Changed

- BREAKING: Updated `lint_lockfile` config to deprecate Node 12 and 17 support since both are end-of-life. Compatible with all current and LTS releases (^14.15.0 || ^16.13.0 || >=18.0.0).
- BREAKING: Update `lint_docker` job to use Alpine linux-based image. (#189)
- BREAKING: Update `code_count` job metrics to be properly Prometheus-formatted metrics with labels. All labels will show as new in the next pipeline. (#190)
- Updated `lint_lockfile` job to `lockfile-lint@4.7.5`
- Updated `node_sbom` job to `@cyclonedx/bom@3.10.0`
- Updated `prepare_release` job to `gitlab-releaser@4.0.0`

## v8.0.2 (2022-05-25)

### Fixed

- Updated GitLab container scanning predefined templates overrides for changes in GitLab 15.0 (no functional changes). (#187)

## v8.0.1 (2022-05-25)

### Fixed

- Updated GitLab predefined templates overrides for changes in GitLab 15.0 (no functional changes). (#187)

## v8.0.0 (2022-05-22)

### Changed

- BREAKING: Update `deploy_branch` (in `Container-Deploy` and `Docker-Deploy`) and `release_check` jobs to use `$CI_DEFAULT_BRANCH` instead of defaulting to "master" (primarily to make compatible with "main"). This is BREAKING for cases where `$CI_DEFAULT_BRANCH` does not represent the latest published branch (e.g. using GitfLow, where `$CI_DEFAULT_BRANCH` would normally be `develop`, but `master`/`main` is the latest published branch). (#141)
- BREAKING: Removed Node 17 from `Node-Version-Tests` collection since end-of-life as of 2022-06-01. (#178)

## v7.0.0 (2022-05-12)

### Changed

- BREAKING: Updated `lint_md` job to pull default configuration file from `config-files` project rather than create from variable `MARKDOWNLINT_RULES`. This default configuration is updated per the latest `markdownlint` rules. If the `MARKDOWNLINT_RULES` variable is overridden it must be changed to reference a file URL or add a `.markdownlint.json` files to the project. (#184)
- BREAKING: Update file `jobs/Lint-NPM-Package.gitlab-ci.yml` to `jobs/Lint-Npm-Package.gitlab-ci.yml` for consistency. (#184)
- BREAKING: Updated `lint_npm_package` default config after `config-files` project move. (#184)
- BREAKING: Update for `config-files` v3.0.0 at new project location
- Added job running the [Sokrates](https://www.sokrates.dev/) code examination tool. (#185)
- Updated `node_sbom` job to `@cyclonedx/bom@3.9.0`

## v6.0.1 (2022-05-01)

### Fixed

- Updated `lint_npm_package` job to remove Node 12 and warn on `engines` (until all modules transitioned off Node 12). (#183)

## v6.0.0 (2022-05-01)

### Changed

- BREAKING: Removed Node 12 from `Node-Version-Tests` collection since end-of-life as of 2022-04-30. (#176)
- Updated to latest dependencies:
  - `node_sbom` job to `@cyclonedx/bom@3.8.0`
  - `pagean` job to `pagean@6.0.9`

### Fixed

- Updated `pnpm` test to trap install errors to avoid failing for unmet peer dependencies. If there are actual install errors, the tests will fail. (#182)

## v5.0.0 (2022-04-22)

### Changed

- BREAKING: Renamed file `jobs/Npm-Lint-Lockfile.gitlab-ci.yml` to `jobs/Lint-Npm-Lockfile.gitlab-ci.yml` for consistency with other files. Only breaking if included directly, all collections were updated. (#173)
- BREAKING: Added `lint_prettier` job and included in Node-Build-Test collection. (#172)
- BREAKING: Added Node 18 tests to `Node-Version-Tests` collection. (#177)
- Pinned `lint_npm_package` job default configuration file to the current version and updated the renovate config to manage updates. (#174)
- Updated `node_sbom` job to `@cyclonedx/bom` to v3.7.0

### Fixed

- Disabled `lint_npm_package` on schedule pipelines for consistency with other lint jobs (#175)
- Updated GitLab template overrides for GitLab 14.10. (#180)

## v4.2.1 (2022-03-31)

### Fixed

- Fixed GitLab `dependency_scanning` template overrides for v14.9 updates. Artifacts include dependency report for all jobs, and SBOM reports if applicable.
- Fixed `pagean_count` needs (#171)

## v4.2.0 (2022-03-28)

### Changed

- Update to `pagean` v6.0.8, including job to improve install count (#170)

## v4.1.0 (2022-03-27)

### Changed

- Updated `prepare_release` job to `gitlab-releaser` v3.0.0. This is not a breaking change for this template as written. **Note this could be a breaking change if the `script` is overridden. See the [release notes](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases/3.0.0) for details on the breaking changes in `gitlab-releaser` v3.0.0.**

## v4.0.0 (2022-03-20)

### Changed

- BREAKING: Updated `lint_lockfile` and `unicode_bidi_test` jobs to install dependency in `before_script` and execute in `script` (previously used `npx` in script). Only breaking if `before_script` is overridden. (#169)
- BREAKING: Updated `lint_lockfile` to validate that the resolved URL matches the package name (`--validate-package-names`)

### Fixed

- Updated jobs to latest dependencies:
  - `lint_lockfile`: Update `lockfile-lint` to v4.7.4
  - `lint_npm_package`: Update `npm-package-json-lint` to v6.3.0
  - `node_sbom`: Update `@cyclonedx/bom` to v3.6.0
  - `unicode_bidi_test`: Update `anti-trojan-source` to v1.4.0

## v3.2.1 (2022-03-07)

### Fixed

- Pinned image digests for `create_release`, `pa11y_ci`, `pagean`

## v3.2.0 (2022-03-07)

### Changed

- Update `node_sbom` job to use the preferred call of `cyclonedx-bom` per v3.5.0 (#168)
- Pinned versions for the following jobs to better handle breaking changes. Updates are now managed by `renovate`.(#167)
  - Node packages: `depcheck`, `json_schema_secure`, `lint_lockfile`, `lint_md`, `lint_npm_package`, `node_sbom`, `npm_check`, `prepare_release`, `unicode_bidi_test`
  - Container images: `create_release`, `pa11y_ci`, `pagean`

## v3.1.0 (2022-02-26)

### Changed

- Disabled `retire-js-dependency_scanning` since deprecated. The functionality is covered by both GitLab's Gemnasium and OWASP Dependency Check. (#166)
- Update `secret_detection` to always run a full scan to resolve MR issues and simplify override logic (#165)

## v3.0.0 (2022-01-28)

### Changed

- BREAKING: Update `lint_npm_package` to not allow failure (#159)
- BREAKING: Updated file name for `unicode_bidi_test` to follow convention (#164)
- BREAKING: Update Hadolint job name to `lint_docker` (and file name) to follow convention (#160)
- Add retry for `node_lts_test_win` job (#162)
- Update `create_release` to GitLab release-cli v0.11.0 (#140)

### Fixed

- Update all GitLab template overrides for v14.7 updates (#163)

## v2.3.1 (2021-12-23)

### Fixed

- Updated `lint_npm_package` with variable to specify the path to lint, which defaults to `./package.json`. (#161)

## v2.3.0 (2021-12-21)

### Added

- Add job to lint NPM package with [`npm-package-json-lint`](https://www.npmjs.com/package/npm-package-json-lint). (#157)

### Fixed

- Updated `pa11y_ci` job for `pa11y-ci-reporter-html` v4 using the `pa11y-ci` reporter interface. (#158)

## v2.2.2 (2021-12-11)

### Fixed

- Fixed `unicode_bidi_test` job to run without dependencies

## v2.2.1 (2021-12-11)

### Fixed

- Fixed `unicode_bidi_test` job name for consistency and added documentation on overrides

## v2.2.0 (2021-12-11)

### Changed

- Added `unicode-bidi-test` job to detect trojan source attacks that employ unicode bidirectional character vulnerabilities to inject malicious code. This job is included in `All-Projects-Slim` collection. (#154)

## v2.1.0 (2021-11-30)

### Changed

- Updated `secret_detection` override to run on tags to include analysis in release evidence collection (#152)
- Update Yarn 2 PnP test (`node_lts_yarn_pnp_test`) to save latest `yarn` artifacts on failure (#153)

## v2.0.0 (2021-10-27)

### Changed

- BREAKING: Updated node LTS jobs to v16 since now the active LTS (#149)
- BREAKING: Updated all node jobs to leverage `.node` template, which is pinned to the active version. (#149)
- Updated GitLab job overrides for GitLab 14.4 (#150)

### Miscellaneous

- Added comments to `code_count` job (#151)

## v1.4.0 (2021-10-20)

### Added

- Added Node 17 to `Node-Version-Tests` collection (#148)

### Changed

- Updated `Npm-Package-Base` collection to remove `npm_check` on schedule pipelines since all npm packages are now using `renovate` for dependency updates. (#147)

## v1.3.1 (2021-10-08)

### Changed

- Update `docker_deploy` and `container_deploy` jobs to output source/destination image names to job log. (#145)

## v1.3.0 (2021-10-07)

### Added

- Added `Container-Build-Test-Deploy` collection, with jobs that leverage [kaniko](https://github.com/GoogleContainerTools/kaniko) for container image builds (`Container-Build`) and [skopeo](https://github.com/containers/skopeo) for deploying container images (`Container-Deploy`). Since these tools do not require Docker-in-Docker, there's a noticeable reduction in pipeline times. The legacy `Docker-Build-Test-Deploy` collection is functionally unchanged, other than extracting common sections of both to the `Container-Base` collection. (#142, #143)

## v1.2.1 (2021-09-24)

### Fixed

- Update GitLab template overrides for v14.3 (#139)
- Pin `create_release` job to `release-cli` v0.9.0 until date issue is v0.10.0 is fixed ([details](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/issues/33))

## v1.2.0 (2021-09-17)

### Added

- Added job to lint PowerShell using [PSScriptAnalyzer](https://github.com/PowerShell/PSScriptAnalyzer) (#138)

### Changed

- Add JSON report to artifacts from `dependency_check` job (#135)

## v1.1.0 (2021-08-29)

### Added

- Added job to lint renovate config files (`renovate.json` or `.gitlab/renovate/json`), and includes in `all-projects-slim` collection (#133)

## v1.0.1 (2021-08-24)

### Fixed

- Update GitLab template overrides for v14.2 (#131)

## v1.0.0 (2021-08-24)

### Changed

- This project has previously not used versioned releases, but to better manage breaking changes establish baseline versioned release at 1.0.0 (#130)
