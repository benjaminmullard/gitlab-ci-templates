# GitLab CI Templates

## Overview

A collection of CI jobs, job templates, and collections of jobs that can be included in other CI pipelines.

Each job runs in one of the following standard stages as shown below.  Each of these may have have a pre- and/or post- stage as required.

```mermaid
graph LR;
  A(prepare)-->B(lint);
  B-->C(build);
  C-->D(test);
  D-->E(deploy);
  E-->F(docker-build);
  F-->G(docker-test);
  G-->H(docker-deploy);
  H-->I(release);
```

## Jobs

The following are individual job files, grouped by the stage in which they execute.  Job with standard variations by pipeline type (e.g. mast branches, other branches, tags) are included together in the same job file.  Where applicable, standard GitLab template jobs are included (with no links). Job files that include multiple integrated jobs that run in multiple stages are listed in all applicable stages.

- `prepare`
  - [Npm-Install.gitlab-ci.yml](./jobs/Npm-Install.gitlab-ci.yml)
- `lint`
  - [Lint-Docker.gitlab-ci.yml](./jobs/Lint-Docker.gitlab-ci.yml)
  - [Lint-Md.gitlab-ci.yml](./jobs/Lint-Md.gitlab-ci.yml)
  - [Lint-Npm-Package.gitlab-ci.yml](./jobs/Lint-Npm-Package.gitlab-ci.yml)
  - [Lint-Nunjucks.gitlab-ci.yml](./jobs/Lint-Nunjucks.gitlab-ci.yml)
  - [Lint-Powershell.gitlab-ci.yml](./jobs/Lint-Powershell.gitlab-ci.yml)
  - [Lint-Renovate.gitlab-ci.yml](./jobs/Lint-Renovate.gitlab-ci.yml)
  - [Lint-Yaml.gitlab-ci.yml](./jobs/Lint-Yaml.gitlab-ci.yml)
  - [Npm-Lint-Css.gitlab-ci.yml](./jobs/Npm-Lint-Css.gitlab-ci.yml)
  - [Npm-Lint-Html.gitlab-ci.yml](./jobs/Npm-Lint-Html.gitlab-ci.yml)
  - [Npm-Lint-Lockfile.gitlab-ci.yml](./jobs/Npm-Lint-Lockfile.gitlab-ci.yml)
  - [Npm-Lint-Js.gitlab-ci.yml](./jobs/Npm-Lint-Js.gitlab-ci.yml)
  - [Npm-Lint-Md.gitlab-ci.yml](./jobs/Npm-Lint-Md.gitlab-ci.yml)
- `test`
  - [Depcheck.gitlab-ci.yml](./jobs/Depcheck.gitlab-ci.yml)
  - `code_quality`, `dependency_scanning`, `license_scanning`, `sast`, `secret_detection`
  - [GitLab-pa11y-ci.gitlab-ci.yml](./jobs/GitLab-pa11y-ci.gitlab-ci.yml)
  - [Json-Schema-Secure.gitlab-ci.yml](./jobs/Json-Schema-Secure.gitlab-ci.yml)
  - [Node-LTS-Test-Win.gitlab-ci.yml](./jobs/Node-LTS-Test-Win.gitlab-ci.yml)
  - [Node-LTS-Test.gitlab-ci.yml](./jobs/Node-LTS-Test.gitlab-ci.yml)
  - [Node-Sbom.gitlab-ci.yml](./jobs/Node-Sbom.gitlab-ci.yml)
  - [Npm-Audit.gitlab-ci.yml](./jobs/Npm-Audit.gitlab-ci.yml)
  - [Npm-Check.gitlab-ci.yml](./jobs/Npm-Check.gitlab-ci.yml) (and [Npm-Check-Schedules-Fail.gitlab-ci.yml](./jobs/Npm-Check-Schedules-Fail.gitlab-ci.yml))
  - [OWASP-Dependency-Check.gitlab-ci.yml](./jobs/OWASP-Dependency-Check.gitlab-ci.yml)
  - [Pagean.gitlab-ci.yml](./jobs/Pagean.gitlab-ci.yml)
  - [Sokrates.gitlab-ci.yml](./jobs/Sokrates.gitlab-ci.yml)
  - [Unicode-Bidi-Test.gitlab-ci.yml](./jobs/Unicode-Bidi-Test.gitlab-ci.yml)
- `pre-deploy`
  - [Npm-Publish.gitlab-ci.yml](./jobs/Npm-Publish.gitlab-ci.yml)
- `deploy`
  - [Npm-Publish.gitlab-ci.yml](./jobs/Npm-Publish.gitlab-ci.yml)
- `docker-build`
  - [Container-Build.gitlab-ci.yml](./jobs/Container-Build.gitlab-ci.yml)
  - [Docker-Build.gitlab-ci.yml](./jobs/Docker-Build.gitlab-ci.yml)
- `docker-test`
  - [Anchore.gitlab-ci.yml](./jobs/Anchore.gitlab-ci.yml)
  - `container_scanning`
  - [Docker-Dive.gitlab-ci.yml](./jobs/Docker-Dive.gitlab-ci.yml)
- `docker-deploy`
  - [Container-Deploy.gitlab-ci.yml](./jobs/Container-Deploy.gitlab-ci.yml)
  - [Docker-Deploy.gitlab-ci.yml](./jobs/Docker-Deploy.gitlab-ci.yml)
- `pre-release`
  - [GitLab-Releaser.gitlab-ci.yml](./jobs/GitLab-Releaser.gitlab-ci.yml)
- `release`
  - [GitLab-Releaser.gitlab-ci.yml](./jobs/GitLab-Releaser.gitlab-ci.yml)
  - [Release-Check.gitlab-ci.yml](./jobs/Release-Check.gitlab-ci.yml)
  - `pages`
- `.post`
  - [Docker-Remove-Image.gitlab-ci.yml](./jobs/Docker-Remove-Image.gitlab-ci.yml)

## Collections

The following are collections of the jobs above, grouped by project type.

- [All-Projects.gitlab-ci.yml](./collections/All-Projects.gitlab-ci.yml) (and [All-Projects-Slim.gitlab-ci.yml](./collections/All-Projects-Slim.gitlab-ci.yml))
- [Container-Base.gitlab-ci.yml](./collections/Container-Base.gitlab-ci.yml)
- [Container-Build-Test-Deploy.gitlab-ci.yml](./collections/Container-Build-Test-Deploy.gitlab-ci.yml)
- [Docker-Build-Test-Deploy.gitlab-ci.yml](./collections/Docker-Build-Test-Deploy.gitlab-ci.yml) (and [Docker-Build-Test-Deploy-Schedules-Fail.gitlab-ci.yml](./collections/Docker-Build-Test-Deploy-Schedules-Fail.gitlab-ci.yml)) (includes `Container-Scanning`)
- [GitLab-Security-Scans.gitlab-ci.yml](./collections/GitLab-Security-Scans.gitlab-ci.yml) (includes `Dependency-Scanning`, `License-Management`, `SAST`)
- [Node-Build-Test.gitlab-ci.yml](./collections/Node-Build-Test.gitlab-ci.yml) (and [Node-Build-Test-Schedules-Fail.gitlab-ci.yml](./collections/Node-Build-Test-Schedules-Fail.gitlab-ci.yml))
- [Node-Version-Tests.gitlab-ci.yml](./collections/Node-Version-Tests.gitlab-ci.yml)
- [Npm-Package-Base.gitlab-ci.yml](./collections/Npm-Package-Base.gitlab-ci.yml)
- [Npm-Publish-Release.gitlab-ci.yml](./collections/Npm-Publish-Release.gitlab-ci.yml)
- [Web-UI-Tests.gitlab-ci.yml](./collections/Web-UI-Tests.gitlab-ci.yml) (and [Web-UI-Tests-With-Lint.gitlab-ci.yml](./collections/Web-UI-Tests-With-Lint.gitlab-ci.yml))

## Templates

The following are templates for standard configurations that are extended in other jobs.

- [Djlint.gitlab-ci.yml](./templates/Djlint.gitlab-ci.yml)
- [Docker-In-Docker.gitlab-ci.yml](./templates/Docker-In-Docker.gitlab-ci.yml)
- [Global Templates.gitlab-ci.yml](./templates/Global=Templates.gitlab-ci.yml)
- [Node-Test.gitlab-ci.yml](./templates/Node-Test.gitlab-ci.yml)
- [Node.gitlab-ci.yml](./templates/Node.gitlab-ci.yml)
- [Python.gitlab-ci.yml](./templates/Python.gitlab-ci.yml)
- [Skopeo.gitlab-ci.yml](./templates/Skopeo.gitlab-ci.yml)
- [Stages.gitlab-ci.yml](./templates/Stages.gitlab-ci.yml)
